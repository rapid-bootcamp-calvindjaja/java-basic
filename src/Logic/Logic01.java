package Logic;

import java.util.Arrays;

public class Logic01 {
    public static void main(String[] args) {
        soal01(10);
        soal02(10);
        soal03(10);
        soal04(10);
        soal05(10);
        soal06(10);
        soal07(10);
        soal08(10);
        soal09(10);
        soal10(10);
    }

    public static void soal01(int n) {
        int[] deret = new int[n];
        // 1, 2, 3, 4, 5, 6, 7, 8, 9
        for (int i = 0; i < deret.length; i++) {
            deret[i] = i + 1;
//            System.out.println(deret[i]);
        }
        System.out.println(Arrays.toString(deret));

    }


    public static void soal02(int n) {
        int[] deret = new int[n];
        // 1, 3, 2, 6, 3, 9, 4, 12, 5
        int deretPertama = 0;
        int deretKedua = 0;
        for (int i = 0; i < deret.length; i++) {
            if (i % 2 == 0) {
                deretPertama++;
                deret[i] = deretPertama;
            } else {
                deretKedua += 3;
                deret[i] = deretKedua;
            }
//            System.out.println(deret[i]);
        }
        System.out.println(Arrays.toString(deret));

    }

    public static void soal03(int n) {
        int[] deret = new int[n];
        // 0, 2, 4, 6, 8, 10, 12, 14, 16
        int deretPertama = 0;
        for (int i = 0; i < deret.length; i++) {
            deret[i] = deretPertama;
            deretPertama += 2;
        }
        System.out.println(Arrays.toString(deret));

    }

    public static void soal04(int n) {
        int[] deret = new int[n];
        // 1, 1, 2, 3, 5, 8, 13, 21, 34
        for (int i = 0; i < deret.length; i++) {
            if (i <= 1) deret[i] = 1;
            else deret[i] = deret[i - 1] + deret[i - 2];
//            System.out.println(deret[i]);
        }
        System.out.println(Arrays.toString(deret));

    }

    public static void soal05(int n) {
        int[] deret = new int[n];
        // 1, 1, 1, 3, 5, 9, 17, 31, 57
        for (int i = 0; i < deret.length; i++) {
            if (i <= 2) deret[i] = 1;
            else deret[i] = deret[i - 1] + deret[i - 2] + deret[i - 3];
//            System.out.println(deret[i]);
        }
        System.out.println(Arrays.toString(deret));

    }

    public static void soal06(int n) {
        int[] deret = new int[n];
        // 1, 1, 1, 3, 5, 9, 17, 31, 57
        int flagArray = 0;
        for (int i = 0; i < Integer.MAX_VALUE; i++) {
            int bil = 0;
            for (int j = 1; j <= i; j++) {
                if (i % j == 0) {
                    bil = bil + 1;
                }
            }
            if (bil == 2) {
                deret[flagArray] = i;
                flagArray++;
            }
            if (flagArray >= deret.length) {
                break;
            }
        }
        System.out.println(Arrays.toString(deret));

    }

    public static void soal07(int n) {
        Character[] deret = new Character[n];
        // 1, 1, 1, 3, 5, 9, 17, 31, 57
        int bil = 65;
        for (int i = 0; i < n; i++) {
            deret[i] = (char) bil;
            bil++;
        }
        System.out.println(Arrays.toString(deret));

    }

    public static void soal08(int n) {
        int[] deret = new int[n];
        // 1, 1, 1, 3, 5, 9, 17, 31, 57
        int bilPertama = 65;
        int bilKedua = 2;
        for (int i = 0; i < n; i++) {
            if(i % 2 == 1){
                deret[i] = bilPertama;
                System.out.print((char) deret[i]+" ");
                bilPertama += 2;
            }
            else {
                deret[i] = bilKedua;
                System.out.print(deret[i]+" ");
                bilKedua += 2;
            }

        }
        System.out.println(Arrays.toString(deret));

    }
    public static void soal09(int n) {
        int[] deret = new int[n];
        // 1, 1, 1, 3, 5, 9, 17, 31, 57
        for (int i = 0; i < n; i++) {
            if(i == 0){
                deret[i] = 1;
            }
            else {
                deret[i] = deret[i-1] * 3;
            }
        }
        System.out.println(Arrays.toString(deret));

    }

    public static void soal10(int n) {
        int[] deret = new int[n];
        // 1, 1, 1, 3, 5, 9, 17, 31, 57
        for (int i = 0; i < n; i++) {
            deret[i] = (int) Math.pow(i,3);
        }
        System.out.println(Arrays.toString(deret));

    }
}

package Logic;

public class Logic02 {
    public static void main(String[] args) {
        int n = 9;
        System.out.println("\nSoal01....");
        soal01(n);
        System.out.println("\nSoal02....");
        soal02(n);
        System.out.println("\nSoal03....");
        soal03(n);
        System.out.println("\nSoal04....");
        soal04(n);
        System.out.println("\nSoal05....");
        soal05(n);
        System.out.println("\nSoal06....");
        soal06(n);
        System.out.println("\nSoal07....");
        soal07(n);
        System.out.println("\nSoal08....");
        soal08(n);
        System.out.println("\nSoal09....");
        soal09(n);
        System.out.println("\nSoal10....");
        soal10(n);
    }

    private static void soal01(int n) {
        int[][] deret = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                deret[i][j] = j + 1;
            }
        }

        for (int b = 0; b < n; b++) {
            for (int k = 0; k < n; k++) {
                if (k == b || k == n - b - 1) {
                    System.out.print(deret[b][k] + "\t");
                } else {
                    System.out.print(" \t");
                }
            }
            System.out.println();
        }
    }

    private static void soal02(int n) {
        int[][] deret = new int[n][n];
        for (int i = 0; i < n; i++) {
            int angka = 1;
            for (int j = 0; j < n; j++) {
                deret[i][j] = angka;
                angka += 2;
            }
        }

        for (int b = 0; b < n; b++) {
            for (int k = 0; k < n; k++) {
                if (k == 0 || b == 0
                        || k == n - 1 || b == n - 1) {
                    System.out.print(deret[b][k] + "\t");
                } else {
                    System.out.print(" \t");
                }
            }
            System.out.println();
        }
    }

    private static void soal03(int n) {
        int[][] deret = new int[n][n];
        for (int i = 0; i < n; i++) {
            int angka = 0;
            for (int j = 0; j < n; j++) {
                deret[i][j] = angka;
                angka += 2;
            }
        }

        for (int b = 0; b < n; b++) {
            for (int k = 0; k < n; k++) {
                if (k == 0 || b == 0
                        || k == n - 1 || b == n - 1
                        || k == b || k == n - b - 1) {
                    System.out.print(deret[b][k] + "\t");
                } else {
                    System.out.print(" \t");
                }
            }
            System.out.println();
        }
    }

    private static void soal04(int n) {
        int[][] deret = new int[n][n];
        for (int i = 0; i < n; i++) {
            int angka = 0;
            for (int j = 0; j < n; j++) {
                deret[i][j] = angka;
                angka += 2;
            }
        }

        for (int b = 0; b < n; b++) {
            for (int k = 0; k < n; k++) {
                if (k == 0 || b == 0
                        || k == n - 1 || b == n - 1
                        || n / 2 == k || n / 2 == b) {
                    System.out.print(deret[b][k] + "\t");
                } else {
                    System.out.print(" \t");
                }
            }
            System.out.println();
        }
    }

    private static void soal05(int n) {
        int[][] deret = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (j <= 2) deret[i][j] = 1;
                else deret[i][j] = deret[i][j - 1] + deret[i][j - 2] + deret[i][j - 3];
            }
        }

        for (int b = 0; b < n; b++) {
            for (int k = 0; k < n; k++) {
                if (k >= b && k >= n - b - 1
                        || k <= b && k <= n - b - 1) {
                    System.out.print(deret[b][k] + "\t");
                } else {
                    System.out.print(" \t");
                }
            }
            System.out.println();
        }
    }

    private static void soal06(int n) {
        int[][] deret = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i <= 1) deret[i][j] = 1;
                else deret[i][j] = deret[i - 1][j] + deret[i - 2][j];
            }
        }

        for (int b = 0; b < n; b++) {
            for (int k = 0; k < n; k++) {
                if (k >= b && k <= n - b - 1
                        || k <= b && k >= n - b - 1) {
                    System.out.print(deret[b][k] + "\t");
                } else {
                    System.out.print(" \t");
                }
            }
            System.out.println();
        }
    }

    private static void soal07(int n) {
        int[][] deret = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (j <= 1 || i <= 1 || i >= n - 2 || j >= n - 2) deret[i][j] = 1;
                else if (j >= i && j < n - i) deret[i][j] = deret[i - 1][n / 2] + deret[i - 2][n / 2];
                else if (j >= n - i - 1 && j <= i) deret[i][j] = deret[n - i - 2][n / 2] + deret[n - i - 3][n / 2];
                else if (j <= n / 2) deret[i][j] = deret[i][j - 1] + deret[i][j - 2];
                else if (j >= n / 2) deret[i][j] = deret[n / 2][n - j - 2] + deret[n / 2][n - j - 3];
            }
        }

        for (int b = 0; b < n; b++) {
            for (int k = 0; k < n; k++) {
                if (k >= b && k >= n - b - 1 && n / 2 <= b
                        || k <= b && k <= n - b - 1 && n / 2 >= b
                        || k >= n / 2 && k <= n - b - 1
                        || k <= n / 2 && k >= n - b - 1) {
                    System.out.print(deret[b][k] + "\t");
                } else {
                    System.out.print(" \t");
                }
            }
            System.out.println();
        }
    }

    private static void soal08(int n) {
        int[][] deret = new int[n][n];
        int[] tempArray = new int[n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (j <= 1) tempArray[j] = 1;
                else tempArray[j] = tempArray[j - 1] + tempArray[j - 2];
            }
        }

        for (int b = 0; b < n; b++) {
            for (int k = 0; k < n; k++) {
                if (k >= n / 2 && k <= n - b - 1
                        || k <= n / 2 && k >= n - b - 1) {

                    if (n / 2 < b) deret[b][k] = tempArray[n - 1 - b];
                    else deret[b][k] = tempArray[b];

                    System.out.print(deret[b][k] + "\t");
                } else {
                    System.out.print(" \t");
                }
            }
            System.out.println();
        }
    }

    private static void soal09(int n) {
        int[][] deret = new int[n][n];

        int kolom = 0;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0 && j == n / 2 || j == 0 && i == n / 2) deret[i][j] = 1;
                else if (j == n / 2 && i <= n / 2) deret[i][j] = deret[i - 1][j] + 2;
                else if (j == n / 2 && i > n / 2) deret[i][j] = deret[i - 1][j] - 2;
            }

            if (i <= n / 2 && i > 0) {
                for (int k = 1; k <= i; k++) {
                    deret[i][n / 2 - k] = deret[i][n / 2] - 2 * k;
                    deret[i][n / 2 + k] = deret[i][n / 2] - 2 * k;
                }
                kolom++;
            } else {
                for (int k = 1; k <= kolom; k++) {
                    deret[i][n / 2 - k] = deret[i][n / 2] - 2 * k;
                    deret[i][n / 2 + k] = deret[i][n / 2] - 2 * k;
                }
                kolom--;
            }

        }
        int nilaiTengah = n / 2;
        for (int b = 0; b < n; b++) {
            for (int k = 0; k < n; k++) {
                if (b - k <= nilaiTengah
                        && k - b <= nilaiTengah
                        && k + b >= nilaiTengah
                        && k + b <= nilaiTengah + n - 1) {
                    System.out.print(deret[b][k] + "\t");
                } else {
                    System.out.print(" \t");
                }
            }
            System.out.println();
        }
    }

    private static void soal10(int n) {
        int[][] deret = new int[n][n];
        int prediksi = n / 2;
        int angkaSatu = 0;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0 && j == 0) deret[i][j] = n;
                else if (j == 0 && i <= n / 2) deret[i][j] = deret[i - 1][j] - 2;
                else if (j == 0 && i > n / 2) deret[i][j] = deret[i - 1][j] + 2;
            }

            if (i <= n / 2) {
                for (int k = 1; k < prediksi + 1; k++) {
                    deret[i][k] = deret[i][k - 1] - 2;
                    deret[i][n - k] = deret[i][k - 1];
                }
                prediksi--;
            } else {
                for (int k = 1; k < prediksi + 3; k++) {
                    deret[i][k] = deret[i][k - 1] - 2;
                    deret[i][n - k] = deret[i][k - 1];
                }
                prediksi++;
            }

            if (i > 0 && i <= n / 2) {
                deret[i][n / 2 - i] = 1;
                deret[i][n / 2 + i] = 1;
                angkaSatu++;
            } else if (i > 0 && i > n / 2) {
                angkaSatu--;
                deret[i][n / 2 - angkaSatu] = 1;
                deret[i][n / 2 + angkaSatu] = 1;
            }

        }
        int nilaiTengah = n / 2;
        for (int b = 0; b < n; b++) {
            for (int k = 0; k < n; k++) {
                if (b - k < nilaiTengah
                        && k - b < nilaiTengah
                        && k + b > nilaiTengah
                        && k + b < nilaiTengah + n - 1) {
                    System.out.print(" \t");
                } else {
                    System.out.print(deret[b][k] + "\t");
                }
            }
            System.out.println();
        }
    }

}
